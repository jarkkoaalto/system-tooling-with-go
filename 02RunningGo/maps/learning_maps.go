package main

import "fmt"

func main() {
	birthdays := map[string]string{
		"Keith": "02/05/1969",
		"Katy":  "04/07/1990",
		"Laura": "09/09/1985",
		"Layla": "10/10/1976",
	}
	fmt.Println(birthdays)

	ages := map[string]int{}
	ages["Keith"] = 59
	ages["Katy"] = 28
	ages["Laura"] = 34
	ages["Layla"] = 43

	fmt.Println(ages, ages["Keith"])
}
