package main

import "fmt"

func main() {
	fmt.Println("Simple String")
	fmt.Println(`
	This is multi-line
	String, that can also contain "quotes".
	`)
	fmt.Println("\u2272")

	fmt.Println('L') // if you use single quote you get ascii number
}
