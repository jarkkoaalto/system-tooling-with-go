package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("Addition:", 1+3)
	fmt.Println("Substraction:", 27-19)
	fmt.Println("Muliplication:", 9*12)
	fmt.Println("Division:", 25.0/4)
	fmt.Println("")
	fmt.Println("Exponents:", math.Pow(20.0, 3))

}
