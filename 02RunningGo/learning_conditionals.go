package main

import "fmt"

func main() {

	ages := map[string]int{}
	ages["Kevin"] = 17
	ages["Kai"] = 33

	if ages["Kevin"] < 18 {
		fmt.Println("Kevin can't vote")
	} else if ages["Kevin"] < 67 {
		fmt.Println("Kevin is not of retirement age")
	} else {
		fmt.Println("Kevin is retairement age")
	}

	switch {
	case ages["Kai"] < 18:
		fmt.Println("Kai can't vote")
	case ages["Kai"] < 67:
		fmt.Println("Kai is not of retirement age")
	default:
		fmt.Println("Kai is retirement age")
	}

}
