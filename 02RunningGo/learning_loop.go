package main

import "fmt"

func main() {
	ages := map[string]int{}
	ages["Kevin"] = 18
	ages["Keith"] = 28
	ages["James"] = 67
	ages["Mick"] = 16
	ages["Lea"] = 5

	for name, age := range ages {
		switch age {
		case 1, 2, 3, 4, 5, 7, 11, 13, 17, 19:
			fmt.Println(name, "has a prime number age")
		case 16:
			fmt.Println(name, "can drive")
		case 18:
			fmt.Println(name, "can vote now!")
		case 67:
			fmt.Println(name, "can retire now")
		default:
			fmt.Println(fmt.Sprintf("There's nothing special about %s's current age.", name))
		}
	}

	fmt.Println("")

	for i := 1; i <= 10; i++ {
		fmt.Println("We're counting:", i)
	}

	fmt.Println("")

	a := 0
	for a < 10 {
		fmt.Println("We're couting (again):", a)
		a++
	}

	fmt.Println("")

	b := 0
	for b < 10 {
		if b%2 == 0 {
			b++
			continue
		} else if b == 5 {
			break
		}
		fmt.Println("We're couting again :", b)
		b++
	}
}
