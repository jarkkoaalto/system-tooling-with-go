package main

import "fmt"

/*
this is multi
line comment
*/

// main in the primary function
func main() {
	fmt.Println("Hello Linux Academy") // trailing comment
}
