package main

import "fmt"

func main() {
	var names [3]string
	names[0] = "Keith"
	names[1] = "Laura"
	names[2] = "Kia"

	fmt.Println(names)
}
