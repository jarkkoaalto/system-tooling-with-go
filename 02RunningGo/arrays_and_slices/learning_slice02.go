package main

import "fmt"

func main() {

	names := make([]string, 4)
	names[0] = "keith"
	names[1] = "Joe"
	names[2] = "Kayla"
	names[3] = "Kyle"

	fmt.Println(names)

}
