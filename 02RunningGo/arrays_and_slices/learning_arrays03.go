package main

import "fmt"

func main() {
	var names [3]string
	names[0] = "Keith"
	names[1] = "Kat"

	fmt.Println(names)
	fmt.Println("names[2] is nil:", names[2])
}
