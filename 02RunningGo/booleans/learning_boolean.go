package main

import "fmt"

func main() {
	fmt.Println("Greate than:", 1 > 2)
	fmt.Println("Less than:", 2 < 3)
	fmt.Println("Greater than OR equal:", 2 >= 2)
	fmt.Println("Less than OR equal:", 4 <= 4)
	fmt.Println("Equivalent:", 4.0 == 4)
	fmt.Println("Not equivalent:", 4.1 != 4.1)

	/*
	 Nothingness, the consept of nothingness comes up pretty frequently in
	 often used along witg comparisons. The value ot represent nothingness
	 in GO is nil
	*/

	if err != nil {
		// handle error
	}
}
