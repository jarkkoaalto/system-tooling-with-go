# System Tooling with Go

OBJECTIVES
By the time you've finished this course you will be able to:

Read, write, and understand Go code
Utilize Go as a primary language for tooling
Develop Go projects from start to finish
Build cross-platform Go binaries