#
# Learning Acrtivities
#
# You've created a tool that will allow you to export user information from a system into 
# JSON or CSV format, and you're ready to deploy it to various machines within your organization. 
# Your organization utilizes Linux, macOS, and FreeBSD machines and you want to have access to 
# your utility on all of the machines. Using Go's ability to build cross-platform applications, 
# you're going to compile three different binaries, one for each operating system.
#
# Note: All of the target machines utilize the amd64 architecture.
#
# Compile an `hr` binary for Linux.
# The primary operating system that the hr utility will be used on is Linux. 
# Compile a binary that can run on Linux systems like your workstation. Call this binary hr.linux.

cd $GOPATH/src/hr
go build -o hr.linux

# Compile an `hr` binary for macOS.
# Compile a binary that can run on macOS systems. Call this binary hr.darwin.
#
# Note: The operating system identifier is darwin.

GOOS=darwin GOARCH=amd64 go build -o hr.darwin

# Compile an `hr` binary for FreeBSD.
# Compile a binary that can run on macOS systems. Call this binary hr.freebsd.

GOOS=freebsd GOARCH=amd64 go build -o hr.freebsd

ls
hr.darwin hr.freebsd hr.linux main.go

./hr.linux
[
	{
		"id": 1000,
		"name": "jarkko",
		"home": /home/jarkko",
		"shell": "/bin/bash"
	},
	{
		"id": 1001,
		"name": "centos",
		"home": "/home/centos",
		"shell": "/bin/bash"
	},
	{
		"id": 1002,
		"name"; "ssm-user",
		"home"; "home/ssm-user",
		"shell": "/bin/bash"
	}
]
